Some scripts used in the study: Transcriptional atlas of intestinal immune cells reveals that neuropeptide α-CGRP modulates group 2 innate lymphoid cell responses, to be published at Immunity, 2019

The single-cell RNA-sequencing UMI count matrix can be downloaded from [GSE124880](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE124880), and cluster labels labels and [scvis](https://bitbucket.org/jerry00/scvis-dev/src/master/) coordinates can be downloaded from [single cell portal](https://portals.broadinstitute.org/single_cell/study/SCP210/fasi-immune-mouse-small-intestine#study-summary). 

We used some R packages that are not in CRAN, e.g., [densitycut](https://bitbucket.org/jerry00/densitycut_dev/src/master/), which can be  installed by the following commands: 

* R CMD build densitycut_dev/  
* R CMD install densitycut_0.0.1.tar.gz  

We made some changes to the [Rphenograph](https://github.com/JinmiaoChenLab/Rphenograph) package, and it can be downloaded from the package folder. You can install Rphenograph similarly as for densitycut

* R CMD build Rphenograph/  
* R CMD install Rphenograph_0.99.1.tar.gz

We made some changes to [Seurat](https://github.com/satijalab/seurat) such that we can use it as a wapper for DE analysis (based on [MAST](https://www.bioconductor.org/packages/release/bioc/html/MAST.html)). It is in the package folder and can be installed:  

* R CMD build seurat/  
* R CMD install Seurat_2.3.3.tar.gz

