scvis map --data_matrix_file ./result/lp_proj_immgen.tsv \
    --out_dir ./result/lp_scvis \
    --pretrained_model_file ./result/pp_scvis/model/perplexity_10_regularizer_0.001_batch_size_1024_learning_rate_0.01_latent_dimension_2_activation_ELU_seed_1_iter_28800.ckpt \
    --config_file ./data/scvis_config.yaml
 
