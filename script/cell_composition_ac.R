rm(list = ls())

library(MASS)
library(dplyr)
library(reshape2)
library(ggplot2)
library(DirichletReg)
library(ggsignif)

setwd('~/work/mouse_small_intestine_immune_cell_atlas')
load('./data/cell_count_statistics.rda')

GetTable = function(pp.count, lp.count, cluster.name) {
  ncluster = length(cluster.name)
  
  cluster.name.add = paste('cluster', cluster.name, sep='.')
  
  name = c(cluster.name.add, 'mouse', 'ac')
  
  pp.cluster = as.data.frame(matrix(0, nrow = nrow(pp.count), ncol=length(name)))
  rownames(pp.cluster) = rownames(pp.count)
  colnames(pp.cluster) = name
  
  name = intersect(colnames(pp.count), name)
  pp.cluster[, name] = pp.count[, name]
  
  # ===== 
  name = c(cluster.name.add, 'mouse', 'ac')
  
  lp.cluster = as.data.frame(matrix(0, nrow = nrow(lp.count), ncol=length(name)))
  rownames(lp.cluster) = rownames(lp.count)
  colnames(lp.cluster) = name
  
  name = intersect(colnames(lp.count), name)
  lp.cluster[, name] = lp.count[, name]
  
  
  pp.cluster = pp.cluster %>% mutate(orig='PP')
  lp.cluster = lp.cluster %>% mutate(orig='LP')
  data = rbind(pp.cluster, lp.cluster)
  
  data$orig = factor(data$orig, levels = c('PP', 'LP'))
  data$ac = factor(data$ac, levels = c('C', 'A'))
  
  return(data)
}

# ==============================================================================
PlotCellGroupEnrich = function(pp.count, lp.count, cluster.name, 
                               cell.group, cluster.to.plot, 
                               use='all', raw=FALSE) {
  x = GetTable(pp.count, lp.count, cluster.name) %>% 
    mutate(ac=ifelse(ac=='A', 'OVA', 'PBS'))
  
  if (use == 'PP') {
    x = x %>% filter(orig == 'PP')
  } else if (use == 'LP') {
    x = x %>% filter(orig == 'LP')
  } else if (use != 'all') {
    stop("use must be in {PP, LP, all}")
  }
  
  total.cell = rowSums(x[, seq(length(cluster.name))])
  names(total.cell) = x$mouse
  
  if (raw == TRUE) {
    xx = melt(x)
    colnames(xx) = c('mouse',  'ac', 'orig', 'cluster', 'ratio')
  } else {
    cluster.name.add = paste('cluster', cluster.name, sep='.')
    x$cellCounts = DR_data(x[, cluster.name.add])
    
    xx = melt(print(x$cellCounts))
    colnames(xx) = c('rownames', 'cluster', 'ratio')
    xx = cbind(xx, experiment=x$mouse, ac=x$ac, orig=x$orig, total=total.cell[x$mouse])
  }
  
  xx$ac = factor(x$ac, levels = c('PBS', 'OVA'))
  xx$cluster = do.call(rbind, strsplit(as.character(xx$cluster), split = '\\.'))[,2]
  xx$cluster = factor(xx$cluster, levels = cluster.name)
  
  xx$group = as.factor(cell.group[xx$cluster])
  
  if (!missing(cluster.to.plot)) {
    xx = xx %>% filter(cluster %in% cluster.to.plot)
  }
  
  p = ggplot(xx, aes(x=cluster, y=ratio))
  
  p = p + geom_boxplot(width=.5, outlier.shape = NA, aes(col = ac))
  
  if (use == 'all') {
    p = p + geom_point(aes(y=ratio, group=ac, shape=orig, col=ac),
               position = position_dodge(width=0.5))
  } else if (use == 'PP') {
    p = p + geom_point(aes(y=ratio, group=ac, col=ac), shape=17, 
                       position = position_dodge(width=0.5))
  } else {
    p = p + geom_point(aes(y=ratio, group=ac, col=ac), shape=16, 
                       position = position_dodge(width=0.5))
  }

  
  p = p + scale_color_manual(values=c('#000000', '#ED3324')) + 
    labs(x="", y="Fraction of cells") +   
    theme(legend.title = element_blank(), 
          legend.direction = "vertical", 
          legend.position = 'right') + 
    guides(fill = guide_legend(reverse = FALSE)) + 
    theme_bw() + 
    theme(panel.border = element_blank(), panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(), 
          axis.line = element_line(colour = "black"))
  
  p
}


# ==============================================================================
TestCellGroupEnrich = function(pp.count, lp.count, cluster.name, 
                               use='all') {
  
  x = GetTable(pp.count, lp.count, cluster.name)
  x$total = rowSums(x[, seq(length(cluster.name))])
  
  x = x %>% filter(total > 0)
  
  if (use == 'PP') {
    x = x %>% filter(orig == 'PP')
  } else if (use == 'LP') {
    x = x %>% filter(orig == 'LP')
  } else if (use != 'all') {
    stop("use must be in {PP, LP, all}")
  }
  
  cluster.name.add = paste('cluster', cluster.name, sep='.')
  
  out = setNames(vector('list', length(cluster.name)), cluster.name.add)
  for (z in cluster.name.add) {
    cat(z, sep='\n')
    xx = x[, c(z, 'ac', 'orig', 'total')]
    colnames(xx) = c('value', 'ac', 'orig', 'total')
    
    if (sum(xx$value) == 0) {
      next 
    }
    
    if (use == 'all') {
      model.nb = glm.nb(value ~  ac + orig + offset(log(total)), data=xx)
    } else {
      model.nb = glm.nb(value ~  ac + offset(log(total)), data=xx)
    }
    
    res = coef(summary(model.nb))
    
    out[[z]] = res
  }
  
  out
}

# ===== PP 
cluster.name = as.character(c(3, 4, 8, 11, 12))
cluster.name = c(cluster.name, as.character(c(6, 14, 21, 24, 31)))
cluster.name = c(cluster.name, as.character(c(22, 26, 36, 44)))
cluster.name = c(cluster.name, as.character(c(25, 32, 33)))

b = TestCellGroupEnrich(pp.count, lp.count, cluster.name, 
                        use = 'PP')
pvalue = sapply(b, function(z) z[2, 4])

pvalue = p.adjust(pvalue, method = 'BH')

p = PlotCellGroupEnrich(pp.count, lp.count, 
                        as.character(as.numeric(cluster.name)), 
                        cell.group, use='PP')

for (z in seq(length(pvalue))) {
  p = p + geom_signif(annotation=format(pvalue[z], digits=2),
                      y_position=0.30, xmin=0.75 + z - 1, 
                      xmax=1.25 + z - 1, 
                      tip_length = 0.00, textsize=1.5)
}

data.frame(pvalue)


# ===== LP 
b = TestCellGroupEnrich(pp.count, lp.count, cluster.name, 
                        use = 'LP')
pvalue = sapply(b, function(z) z[2, 4])

pvalue = p.adjust(unlist(pvalue), method = 'BH')
pvalue = pvalue[names(b)]
names(pvalue) = names(b)

p = PlotCellGroupEnrich(pp.count, lp.count, 
                        as.character(as.numeric(cluster.name)), 
                        cell.group, use='LP')


for (z in seq(length(pvalue))) {
  p = p + geom_signif(annotation=format(pvalue[z], digits=2),
                      y_position=0.30, xmin=0.75 + z - 1, 
                      xmax=1.25 + z - 1, 
                      tip_length = 0.00, textsize=1.5)
}



# ===== PP + LP
b = TestCellGroupEnrich(pp.count, lp.count, cluster.name, 
                        use = 'all')
pvalue = sapply(b, function(z) z[2, 4])

pvalue = p.adjust(unlist(pvalue), method = 'BH')
pvalue = pvalue[names(b)]
names(pvalue) = names(b)

p = PlotCellGroupEnrich(pp.count, lp.count, 
                        as.character(as.numeric(cluster.name)), 
                        cell.group, use='all')


for (z in seq(length(pvalue))) {
  p = p + geom_signif(annotation=format(pvalue[z], digits=2),
                      y_position=0.30, xmin=0.75 + z - 1, 
                      xmax=1.25 + z - 1, 
                      tip_length = 0.00, textsize=1.5)
}






